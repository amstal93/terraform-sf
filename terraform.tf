## Crear cuenta de servicio con permiso editor

## Conexion a proveedor
provider google {
  credentials = file("terraform-sa.json")
  project = var.project_id
  region = var.region
}

########################
#####  Habilitar  ######
# Kubernetes Engine API
# Cloud DNS API
# Cloud SQL Admin API
########################

## Creacion de REDES
resource "google_compute_network" "network" {
  name = "net"
  auto_create_subnetworks = false
}
resource "google_compute_subnetwork" "subnetwork_front" {
  name = "front-subnet"
  ip_cidr_range = var.ip_cidr_range_front
  region = var.region
  network = google_compute_network.network.self_link
}
resource "google_compute_subnetwork" "subnetwork_back" {
  name = "back-subnet"
  ip_cidr_range = var.ip_cidr_range_back
  region = var.region
  network = google_compute_network.network.self_link
}

## Creacion de DNS
resource "google_dns_managed_zone" "zona_publica" {
  name = "public"
  dns_name = "sgarces.cl."
  description = "Zona publica"
  visibility = "public"
  dnssec_config {
    state = "off"
  }
}
resource "random_id" "rnd" {
  byte_length = 4
}

## Creacion de cluster GKE
resource "google_container_cluster" "frontend" {
  name = "frontend"
  location = var.region
  enable_autopilot = true
  network = google_compute_network.network.id
  subnetwork = google_compute_subnetwork.subnetwork_front.id
  node_config {
    preemptible = true
    machine_type = "e2-small"
  }
}
resource "google_container_cluster" "backend" {
  name = "backend"
  location = var.region
  enable_autopilot = true
  network = google_compute_network.network.id
  subnetwork = google_compute_subnetwork.subnetwork_back.id
  node_config {
    preemptible = true
    machine_type = "e2-medium"
  }
}

## Creacion de BBDD
resource "google_sql_database" "database" {
  name = "sergiofabianDB"
  instance = google_sql_database_instance.primaria.name
}
resource "google_sql_database_instance" "primaria" {
  name = "primaria"
  database_version = "POSTGRES_11"
  region = var.region
  settings {
    tier = "db-f1-micro"
  }
  deletion_protection = "true"
}
