#### GCLOUD
* Conectar nueva cuenta de servicio
    * gcloud auth activate-service-account "service-account" --key-file="key.json" --project "project-id"
* Login a cuenta ya configurada (Interactivo)
    * gcloud auth login
* Configurar kubectl para conectarse a gcloud
    * gcloud container clusters get-credentials "nombre-cluster" --zone "ubicacion" --project "project-id"
#### K8S
* Aplicar manifiesto
    * kubectl apply -f namespace.yaml
    * kubectl apply -f deployment.yaml
* Seleccionar namespace
    * kubectl get ns
    * kubectl config set-context --current --namespace=sitio-web
* Borrar servicio
    * kubectl delete service http
#### ISTIO
* Instalar istio desde helm
    * helm template install/kubernetes/helm/istio --name istio --namespace istio-system > $HOME/istio.yaml
    * kubectl create namespace istio-system
    * kubectl apply -f $HOME/istio.yaml
* Inyeccion de envoy
    * kubectl label namespace default istio-injection=enabled
#### DOCKER
* Crear imagen
    * docker build --tag sergiofabiancl/despliegue:v0.1.6 -f Dockerfile .
* Arrancar maquina creada
    * docker run -d --name web -p 80:80 sergiofabiancl/despliegue:v0.1.6
* Listar imagenes activas
    * docker ps -a
* Acceder a un contenedor
    * docker exec -i -t "id" /bin/bash
* Detener y Borrar contenedor 
    * docker stop "id"
    * docker rm "id"
* Borrar imagen guardada
    * docker rmi sergiofabiancl/despliegue:"tag"
* Login en docker hub
    * docker login -u sergiofabiancl
* Subir imagen al hub
    * docker push sergiofabiancl/despliegue:v0.1.6
