# Globales
variable "project_id" {
  default = "nubepersonal-01"
}
variable "region" {
  default = "us-central1"
}

# Red
variable "ip_cidr_range_front" {
  default = "10.100.0.0/16"
}
variable "ip_cidr_range_back" {
  default = "10.200.0.0/16"
}